//
//  ViewController_Add.swift
//  Energy
//
//  Created by Breno Aquino on 08/04/17.
//  Copyright © 2017 Breno Aquino. All rights reserved.
//

import UIKit

class ViewController_Add: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var input_name: UITextField!
    @IBOutlet weak var input_qtd: UITextField!
    @IBOutlet weak var input_time: UITextField!
    @IBOutlet weak var input_power: UITextField!
    @IBOutlet weak var output_table_view: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func add_electronic() {
        if input_name.text == "" || input_qtd.text == "" || input_time.text == "" || input_power.text == "" {
            var array_i = [1, 2, 3, 4, 5]
            print(array_i)
            var j: Int = 0
            for i in 0...(array_i.count-1) {
                array_i[i] += 30
            }
            
            j += 10
            print(array_i)
            print(j)
            
            return
        }
        
        System.instance.add_elec(name: input_name.text!, qtd: Int(input_qtd.text!)!, time: Int(input_time.text!)!, power: Float(input_power.text!)!)
        
        input_name.text = ""
        input_qtd.text = ""
        input_time.text = ""
        input_power.text = ""
        
        output_table_view.reloadData()
        input_power.resignFirstResponder()
        
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return System.instance.array_elec.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = output_table_view.dequeueReusableCell(withIdentifier: "AddCell", for: indexPath)
        cell.textLabel?.text = System.instance.array_elec[indexPath.row].name
        return cell
    }
}
