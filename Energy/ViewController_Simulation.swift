//
//  ViewController_Simulation.swift
//  Energy
//
//  Created by Breno Aquino on 09/04/17.
//  Copyright © 2017 Breno Aquino. All rights reserved.
//

import UIKit

class ViewController_Simulation: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var output_tableView: UITableView!
    @IBOutlet weak var output_consumption: UILabel!
    @IBOutlet weak var output_pay: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        output_consumption.text = "\(System.instance.total_consumption())"
        output_pay.text = "\(System.instance.total_pay())"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return System.instance.array_elec.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = output_tableView.dequeueReusableCell(withIdentifier: "SimuCell", for: indexPath) as! SimuCell
        
        cell.name.text = System.instance.array_elec[indexPath.row].name
        cell.qtd.text = "Quantidade: " + String(System.instance.array_elec[indexPath.row].qtd)
        cell.time.text = "Tempo: " + String(System.instance.array_elec[indexPath.row].time)
        cell.power.text = "Potencia: " + String(System.instance.array_elec[indexPath.row].power)
        cell.consumption.text = "Consumption: " + String(System.instance.array_elec[indexPath.row].consumption)
        cell.pay.text = "Custo: " + String(System.instance.array_elec[indexPath.row].consumption * 0.5)
        
        return cell
    }
    
}
