//
//  System.swift
//  Energy
//
//  Created by Breno Aquino on 08/04/17.
//  Copyright © 2017 Breno Aquino. All rights reserved.
//

import Foundation

class System {
    
    static let instance = System()
    
    var array_elec: [Electronic]
    
    init() {
        array_elec = [Electronic]()
    }
    
    /* Medias */
    
    func avg_qtd() -> Float {
        var toReturn: Float = 0
        for elec in array_elec {
            toReturn += Float(elec.qtd)
        }
        toReturn /= Float(array_elec.count)
        return toReturn
    }
    
    func avg_time() -> Float {
        var toReturn: Float = 0
        for elec in array_elec {
            toReturn += Float(elec.time)
        }
        toReturn /= Float(array_elec.count)
        return toReturn
    }
    
    func avg_power() -> Float {
        var toReturn: Float = 0
        for elec in array_elec {
            toReturn += Float(elec.power)
        }
        toReturn /= Float(array_elec.count)
        return toReturn
    }
    
    func avg_consumption() -> Float {
        var toReturn: Float = 0
        for elec in array_elec {
            toReturn += Float(elec.consumption)
        }
        toReturn /= Float(array_elec.count)
        return toReturn
    }
    
    /* -- */
    
    /* Adiconar/Listar */
    
    func add_elec(name _name_: String, qtd _qtd_: Int, time _time_: Int, power _power_: Float) {
        var isHere: Bool = false
        var toAdd = Electronic()
        
        for elec in array_elec {
            if elec.name == _name_ {
                isHere = true
                toAdd = elec
                break
            }
        }

        if !isHere {
            toAdd = Electronic(name: _name_, qtd: _qtd_, time: _time_, power: _power_)
            array_elec.append(toAdd)
        }
        else {
            if toAdd.time == _time_ && toAdd.power == _power_ {
                toAdd.qtd! += _qtd_
            }
            else {
                print("Erro")
            }
        }
    }
    
    func print_elec() {
        for single_elec in array_elec {
            single_elec.print_elec()
        }
    }
    
    /* --- */
    
    /* Informações Totais */
    func total_consumption() -> Float {
        var toReturn: Float = 0
        for elec in array_elec {
            toReturn += Float(elec.consumption)
        }
        return toReturn
    }
    
    func total_pay() -> Float {
        return (total_consumption() * 0.5)
    }
    
    /* --- */
}
