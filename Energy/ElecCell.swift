//
//  ElecCell.swift
//  Energy
//
//  Created by Breno Aquino on 09/04/17.
//  Copyright © 2017 Breno Aquino. All rights reserved.
//

import UIKit

class ElecCell : UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var qtd: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var power: UILabel!
    
}

class SimuCell : UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var qtd: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var power: UILabel!
    @IBOutlet weak var consumption: UILabel!
    @IBOutlet weak var pay: UILabel!
    
}
