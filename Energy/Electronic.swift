//
//  Electronic.swift
//  Energy
//
//  Created by Breno Aquino on 08/04/17.
//  Copyright © 2017 Breno Aquino. All rights reserved.
//

import Foundation

class Electronic {
    var name :String!
    var qtd :Int!
    var time :Int!
    var power :Float!
    var consumption: Float!
    
    init() {
        self.name = ""
        self.qtd = 0
        self.time = 0
        self.power = 0
    }
    
    init(name _name_: String, qtd _qtd_: Int, time _time_: Int, power _power_: Float) {
        self.name = _name_
        self.qtd = _qtd_
        self.time = _time_
        self.power = _power_
        self.consumption = self.power * Float(self.qtd * self.time) * 0.5
    }
    
    func print_elec() {
        print("Nome " + name)
        print("Quantidade \(qtd)")
        print("Tempo \(time)")
        print("Potencia \(power)")
    }
}
