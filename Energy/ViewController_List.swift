//
//  ViewController_List.swift
//  Energy
//
//  Created by Breno Aquino on 09/04/17.
//  Copyright © 2017 Breno Aquino. All rights reserved.
//

import UIKit

class ViewController_List: UITableViewController {
    
    @IBOutlet var output_tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return System.instance.array_elec.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = output_tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath) as! ElecCell
        cell.name.text = System.instance.array_elec[indexPath.row].name
        cell.qtd.text = "Quantidade: " + String(System.instance.array_elec[indexPath.row].qtd)
        cell.time.text = "Tempo: " + String(System.instance.array_elec[indexPath.row].time)
        cell.power.text = "Potencia: " + String(System.instance.array_elec[indexPath.row].power)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            System.instance.array_elec.remove(at: indexPath.row)
            output_tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
}
